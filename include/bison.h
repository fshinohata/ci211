#ifndef __PASCAL_COMPILER_BISON__
#define __PASCAL_COMPILER_BISON__

#include "const.h"

typedef enum DataType DataType;
typedef enum ParameterType ParameterType;
typedef enum SymbolTableEntryType SymbolTableEntryType;

typedef struct ProcedureParameter ProcedureParameter;
typedef struct SymbolTableEntry_base SymbolTableEntry_base;
typedef struct SymbolTableEntry_label SymbolTableEntry_label;
typedef struct SymbolTableEntry_variable SymbolTableEntry_variable;
typedef struct SymbolTableEntry_parameter SymbolTableEntry_parameter;
typedef struct SymbolTableEntry_procedure SymbolTableEntry_procedure;
typedef struct SymbolTableEntry_function SymbolTableEntry_function;
typedef union SymbolTableEntry SymbolTableEntry;

typedef struct SymbolTable SymbolTable;

enum DataType
{
    UNDEFINED,
    INTEGER,
    BOOLEAN
};

enum ParameterType
{
    BOTH, /* used in validation only */
    BY_COPY,
    BY_REFERENCE
};

enum SymbolTableEntryType
{
    SYMBOL_ANY, /* used in search only */
    SYMBOL_VARIABLE,
    SYMBOL_LABEL,
    SYMBOL_PARAMETER,
    SYMBOL_PROCEDURE,
    SYMBOL_FUNCTION
};

struct ProcedureParameter
{
    DataType type;
    ParameterType parameterType;
};

struct SymbolTableEntry_base
{
    SymbolTableEntryType category;
    char identifier[TOKEN_SIZE];
    int lexicalLevel;
    int offset;
};

struct SymbolTableEntry_label
{
    SymbolTableEntry_base base;
    char label[LABEL_SIZE];
};

struct SymbolTableEntry_variable
{
    SymbolTableEntry_base base;
    DataType type;
};

struct SymbolTableEntry_parameter
{
    SymbolTableEntry_base base;
    DataType type;
    ParameterType parameterType;
};

struct SymbolTableEntry_procedure
{
    SymbolTableEntry_base base;
    char label[LABEL_SIZE];
    ProcedureParameter *parameters;
    int parametersLength;
};

struct SymbolTableEntry_function
{
    SymbolTableEntry_base base;
    char label[LABEL_SIZE];
    ProcedureParameter *parameters;
    int parametersLength;
    DataType returnType;
};

union SymbolTableEntry
{
    SymbolTableEntry_base base;
    SymbolTableEntry_label label;
    SymbolTableEntry_variable variable;
    SymbolTableEntry_parameter parameter;
    SymbolTableEntry_procedure procedure;
    SymbolTableEntry_function function;
};

struct SymbolTable
{
    SymbolTableEntry *stack;
    int head;
    int allocated;
};

/* functions */
void SymbolTableEntry_addParameter(SymbolTableEntry *entry, ParameterType parameterType);
void SymbolTableEntry_updateParametersTypes(SymbolTableEntry *entry, DataType dataType);

SymbolTableEntry createSymbolTableEntry_function(char identifier[TOKEN_SIZE], int lexicalLevel, char label[LABEL_SIZE]);
SymbolTableEntry createSymbolTableEntry_procedure(char identifier[TOKEN_SIZE], int lexicalLevel, char label[LABEL_SIZE]);
SymbolTableEntry createSymbolTableEntry_parameter(char identifier[TOKEN_SIZE], int lexicalLevel, ParameterType parameterType);
SymbolTableEntry createSymbolTableEntry_variable(char identifier[TOKEN_SIZE], int lexicalLevel, int offset);
SymbolTableEntry createSymbolTableEntry_label(char identifier[TOKEN_SIZE], int lexicalLevel, char label[LABEL_SIZE]);

SymbolTable *createSymbolTable();
void deleteSymbolTable(SymbolTable *st);
void SymbolTable_reallocIfNecessary(SymbolTable *st);
SymbolTableEntry *SymbolTable_push(SymbolTable *st, SymbolTableEntry entry);
void SymbolTable_pop(SymbolTable *st);
void SymbolTable_decreaseLexicalLevelTo(SymbolTable *st, int lexicalLevel);

int SymbolTable_symbolExists(SymbolTable *st, const char identifier[TOKEN_SIZE], SymbolTableEntryType category, int lexicalLevel);
SymbolTableEntry *SymbolTable_search(SymbolTable *st, const char identifier[TOKEN_SIZE], SymbolTableEntryType category);

void SymbolTable_updateUndefinedTypes(SymbolTable *st, SymbolTableEntryType category, DataType type);
int SymbolTable_updateParametersOffsets(SymbolTable *st);
void SymbolTable_updateFunctionOffset(SymbolTableEntry *f, int offset);
void SymbolTable_updateFunctionReturnType(SymbolTableEntry *f, DataType returnType);

#endif