#ifndef __PASCAL_COMPILER__
#define __PASCAL_COMPILER__

#include "const.h"
#include "flex.h"
#include "bison.h"

#define MAXIMUM_LEXICAL_LEVEL 128
#define MAXIMUM_STACK_SIZE 128

#ifdef _DEBUG
    #define VERBOSE(...) fprintf(stderr, __VA_ARGS__)
#else
    #define VERBOSE(...) {}
#endif

typedef struct LabelStack LabelStack;
typedef struct LabelStackItem LabelStackItem;

typedef struct Compiler Compiler;
typedef struct CompilerData CompilerData;

struct LabelStackItem
{
    char **names;
    int namesLength;
};

struct LabelStack
{
    int head;
    LabelStackItem *stack;
};

struct CompilerData
{
    Symbol symbol;
    int line;
    int lexicalLevel;

    SymbolTableEntry *holder; /* holds procedures/functions on declaration/call */
    int parameterCount[MAXIMUM_LEXICAL_LEVEL]; /* number of parameters of current lexical level */
    int isCall; /* tells if the actual context is a procedure call (0 = false, 1 = true) */

    SymbolTableEntry *POF[MAXIMUM_LEXICAL_LEVEL]; /* POF = procedure or function, stores the entry of the current procedure/function being processed for the lexical levels */
    int currentPOF; /* lexical level 0 is not used */

    int offset[MAXIMUM_LEXICAL_LEVEL]; /* variable offset */
    int counters[MAXIMUM_STACK_SIZE];
    int currentCounter;

    SymbolTableEntry *entry; /* holds current read entry */
    char token[TOKEN_SIZE];
    char lastToken[TOKEN_SIZE]; /* for assignment rule bug */

    char callTargets[MAXIMUM_STACK_SIZE][TOKEN_SIZE];
    int currentCallTarget;

    char assignmentTargets[MAXIMUM_STACK_SIZE][TOKEN_SIZE];
    int currentAssignmentTarget;
    LabelStack *labels;
};

struct Compiler
{
    CompilerData *context;
    SymbolTable *symbolTable;
};

Compiler *compiler;

/* print rules */
void DEBUG();
void INPP();
void PARA();
void AMEM();
void DMEM();
void SOMA();
void SUBT();
void MULT();
void DIVI();
void CONJ();
void DISJ();
void CRCT();
void CRVL();
void CRVI();
void CREN();
void ARMZ();
void ARMI();
void CMDG();
void CMME();
void CMEG();
void CMIG();
void CMAG();
void CMMA();
void NEGA();
void DSVS(int n);
void DSVF(int n);
void LABEL(int n);
void ENPR();
void CHPR();
void RTPR();
void LEIT();
void IMPR();
void ENRT();
void DSVR();

/* helpers */
void initCompiler();
void deleteCompiler();

void enterCall();
void exitCall();
void saveFunctionSpace(); /* gambiarra :) */
void load();
void store();

void addCounter();
void discardCounter();
void count();

void increaseLexicalLevel();
void decreaseLexicalLevel();

void saveCallTarget();
void discardCallTarget();

void saveAssignmentTarget();
void discardAssignmentTarget();

void insertProcedure();
void insertFunction();
void insertParameter();
void insertVariable();
void insertLabel();

void updateParametersDataTypes();
int updateParametersOffsets();
void updateParametersAndFunction();
void updateVariablesDataTypes();
void updateFunctionReturnType();

void newLabels(int n);
void discardLabels();

void assertFunctionParameterCount();
void assertFunctionHasNoParameters();
void assertFunctionParameterIs(ParameterType parameterType, DataType dataType);
void assertProcedureParameterCount();
void assertProcedureHasNoParameters();
void assertProcedureParameterIs(ParameterType parameterType, DataType dataType);
void assertFunctionAssignmentIsValid();
void assertLabelIsValid();

void error(const char *message, ...);

#endif