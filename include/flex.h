#ifndef __PASCAL_COMPILER_FLEX__
#define __PASCAL_COMPILER_FLEX__

#include "const.h"

typedef enum Symbol {
    /* special */
    S_NONE,
    S_DEBUG,
    S_READ,
    S_WRITE,

    /* keywords */
    S_PROGRAM,
    S_LABEL,
    S_VAR,
    S_VAR_TYPE,
    S_PROCEDURE,
    S_FUNCTION,
    S_BEGIN,
    S_END,
    S_GOTO,

    /* identifier types */
    S_IDENTIFIER_VARIABLE_UNDEFINED,
    S_IDENTIFIER_VARIABLE_INTEGER,
    S_IDENTIFIER_VARIABLE_BOOLEAN,
    S_IDENTIFIER_PROCEDURE,
    S_IDENTIFIER_FUNCTION,

    /* separators */
    S_COMMA,
    S_SEMICOLON,
    S_COLON,
    S_DOT,
    S_LEFT_PARENTHESIS,
    S_RIGHT_PARENTHESIS,

    /* conditional and comparison */
    S_IF,
    S_THEN,
    S_ELSE,
    S_NOT_EQUAL,
    S_LESS,
    S_LESS_OR_EQUAL,
    S_EQUAL,
    S_GREATER_OR_EQUAL,
    S_GREATER,

    /* loops */
    S_WHILE,
    S_DO,
    
    /* logical symbols */
    S_AND,
    S_OR,
    S_NOT,
    S_TRUE,
    S_FALSE,

    /* math */
    S_PLUS,
    S_MINUS,
    S_SLASH,
    S_ASTERISK,
    S_DIV,

    /* program */
    S_ASSIGNMENT,
    S_NUMBER,
    S_IDENTIFIER
} Symbol;

enum yytokentype registerToken(Symbol s, enum yytokentype t);
void dontReturnTypes();
void doReturnTypes();

#endif