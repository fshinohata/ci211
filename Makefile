#!/usr/bin/make
# folders
_SRC = src
_INC = include

#
CC = gcc
CCFLAGS = -I $(_INC) -I. -ll -ly -lc

.PHONY: all debug_test test clean

all: compiler

compiler:
	flex $(_SRC)/flex.l
	bison $(_SRC)/bison.y -d -v
	gcc lex.yy.c bison.tab.c $(_SRC)/*.c $(CCFLAGS) -o compiler

debug: CCFLAGS += -D_DEBUG
debug: all

debug_test: CCFLAGS += -D_DEBUG
debug_test: test
test: all
	@ ./test.sh

clean:
	rm -f *.output *.tab.* lex.yy.c compiler
