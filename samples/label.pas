program test(input, output);
{$goto on}
label 1;
var x: integer;
    b: boolean;
    procedure p(var n: integer);
    label 1, 2;
    begin
        if n = 0
        then goto 1
        else goto 2;
        if n = 1
        then
1:         n := 100
        else
2:         n := 200;
    end;
begin
    x := 1;
    b := true;
    p(x);
    writeln(x);
1:  x := 0;
    p(x);
    writeln(x);
    if b
    then begin
        b := false;
        goto 1
    end;
end.