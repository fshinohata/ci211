program bools(input, output);
var a, b, c, d: boolean;
begin
    a := true; b := true; c := true; d := false;
    if not (a and b) or c and not d = 1
    then
        writeln('It works!')
end.