#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdarg.h>
#include "compiler.h"

#define MINIMUM_LABEL_STACK_SIZE 128

const char *DataTypeName[] = {
    "UNDEFINED",
    "INTEGER",
    "BOOLEAN"
};

const char *ParameterTypeName[] = {
    "BOTH",
    "BY_COPY",
    "BY_REFERENCE"
};

static char *LabelStack_get(LabelStack *l, int k)
{
    return l->stack[l->head].names[k - 1];
}

void LabelStack_push(LabelStack *l, int num)
{
    static int n = 0;
    l->head++;

    l->stack[l->head].names = (char **)malloc(num * sizeof(char *));
    for (int i = 0; i < num; i++)
    {
        l->stack[l->head].names[i] = (char *)calloc(LABEL_SIZE, sizeof(char));
        sprintf(l->stack[l->head].names[i], "R%02d", n++);
    }
}

void LabelStack_pop(LabelStack *l)
{
    for (int i = 0; i < l->stack[l->head].namesLength; i++)
    {
        free(l->stack[l->head].names[i]);
    }

    free(l->stack[l->head].names);
    l->head--;
}

LabelStack *createLabelStack()
{
    LabelStack *l = (LabelStack *)malloc(sizeof(LabelStack));
    l->head = -1;
    l->stack = (LabelStackItem *)malloc(MINIMUM_LABEL_STACK_SIZE * sizeof(LabelStackItem));
    return l;
}

void deleteLabelStack(LabelStack *l)
{
    free(l->stack);
    free(l);
}

CompilerData *createCompilerData()
{
    CompilerData *cd = (CompilerData *)malloc(sizeof(CompilerData));
    cd->lexicalLevel = 0;
    cd->line = 0;
    cd->symbol = S_NONE;
    cd->currentAssignmentTarget = -1;
    cd->currentCallTarget = -1;
    cd->currentCounter = -1;
    cd->labels = createLabelStack();
    cd->isCall = 0;
    cd->currentPOF = 0;
    memset(cd->POF, 0, MAXIMUM_LEXICAL_LEVEL);
    memset(cd->token, '\0', TOKEN_SIZE);
    memset(cd->offset, -1, MAXIMUM_LEXICAL_LEVEL * sizeof(int));
    return cd;
}

void deleteCompilerData(CompilerData *cd)
{
    deleteLabelStack(cd->labels);
    free(cd);
}

Compiler *createCompiler()
{
    Compiler *c = (Compiler *)malloc(sizeof(Compiler));
    c->context = createCompilerData();
    c->symbolTable = createSymbolTable();
    return c;
}

void deleteCompiler()
{
    deleteCompilerData(compiler->context);
    deleteSymbolTable(compiler->symbolTable);
    free(compiler);
}

void initCompiler()
{
    compiler = createCompiler();
}

/* context control and context-dependent */
void enterCall()
{
    compiler->context->isCall = 1;
}

void exitCall()
{
    compiler->context->isCall = 0;
}

void saveFunctionSpace()
{
    /* gambiarra :) */
    addCounter();
    count();
    AMEM();
    discardCounter();
}

void load()
{
    SymbolTableEntry *entry = compiler->context->entry;
    SymbolTableEntry *callTarget = SymbolTable_search(compiler->symbolTable, compiler->context->callTargets[compiler->context->currentCallTarget], SYMBOL_ANY);
    if (compiler->context->isCall == 1)
    {
        ProcedureParameter current = callTarget->base.category == SYMBOL_PROCEDURE
                                     ? callTarget->procedure.parameters[compiler->context->counters[compiler->context->currentCounter] - 1]
                                     : callTarget->function.parameters[compiler->context->counters[compiler->context->currentCounter] - 1];
        if (current.parameterType == BY_COPY)
        {
            if (entry->base.category == SYMBOL_VARIABLE)
                CRVL();
            else if (entry->base.category == SYMBOL_PARAMETER)
            {
                if (entry->parameter.parameterType == BY_COPY)
                    CRVL();
                else
                    CRVI();
            }
        }
        else if (current.parameterType == BY_REFERENCE)
        {
            if (entry->base.category == SYMBOL_VARIABLE)
                CREN();
            else if (entry->base.category == SYMBOL_PARAMETER)
            {
                if (entry->parameter.parameterType == BY_COPY)
                    CREN();
                else if (entry->parameter.parameterType == BY_REFERENCE)
                    CRVL();
            }
        }
    }
    else
    {
        if (entry->base.category == SYMBOL_VARIABLE)
            CRVL();
        else if (entry->base.category == SYMBOL_PARAMETER)
        {
            if (entry->parameter.parameterType == BY_COPY)
                CRVL();
            else if(entry->parameter.parameterType == BY_REFERENCE)
                CRVI();
        }
    }
}

void store()
{
    SymbolTableEntry *entry = SymbolTable_search(compiler->symbolTable, compiler->context->assignmentTargets[compiler->context->currentAssignmentTarget], SYMBOL_ANY);
    if (entry->base.category == SYMBOL_VARIABLE || entry->base.category == SYMBOL_FUNCTION)
        ARMZ();
    else if (entry->base.category == SYMBOL_PARAMETER)
    {
        if (entry->parameter.parameterType == BY_COPY)
            ARMZ();
        else if (entry->parameter.parameterType == BY_REFERENCE)
            ARMI();
    }
}



/* counting */
void addCounter()
{
    compiler->context->currentCounter++;
    compiler->context->counters[compiler->context->currentCounter] = 0;
}

void discardCounter()
{
    compiler->context->currentCounter--;
}

void count()
{
    compiler->context->counters[compiler->context->currentCounter]++;
}

void increaseLexicalLevel()
{
    compiler->context->lexicalLevel++;
    compiler->context->offset[compiler->context->lexicalLevel] = -1;
    compiler->context->parameterCount[compiler->context->lexicalLevel] = 0;
}

void decreaseLexicalLevel()
{
    compiler->context->currentPOF--;
    compiler->context->lexicalLevel--;
    SymbolTable_decreaseLexicalLevelTo(compiler->symbolTable, compiler->context->lexicalLevel);
}



/* Symbols (validation, insertion, remotion, etc) */
void saveCallTarget()
{
    VERBOSE("Saving call target name '%s'\n", compiler->context->token);
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_PROCEDURE);
    if (match == NULL)
    {
        match = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_FUNCTION);
        if (match == NULL)
        {
            error("Symbol '%s' is not a function/procedure (line: %d)\n", compiler->context->token, compiler->context->line);
        }
    }
    compiler->context->currentCallTarget++;
    memset(compiler->context->callTargets[compiler->context->currentCallTarget], '\0', TOKEN_SIZE);
    strncpy(compiler->context->callTargets[compiler->context->currentCallTarget], compiler->context->token, TOKEN_SIZE);
}

void discardCallTarget()
{
    VERBOSE("Discarding call target '%s'\n", compiler->context->callTargets[compiler->context->currentCallTarget]);
    compiler->context->currentCallTarget--;
}

void saveAssignmentTarget()
{
    char *token = 0 == strncmp(compiler->context->token, ":=", 2) ? compiler->context->lastToken : compiler->context->token;
    VERBOSE("Saving assignment target name '%s'\n", token);
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, token, SYMBOL_VARIABLE);
    if (match == NULL)
    {
        match = SymbolTable_search(compiler->symbolTable, token, SYMBOL_PARAMETER);
        if (match == NULL)
        {
            match = SymbolTable_search(compiler->symbolTable, token, SYMBOL_FUNCTION);
            if (match == NULL)
            {
                match = SymbolTable_search(compiler->symbolTable, token, SYMBOL_ANY);
                if (match == NULL)
                {
                    error("Undeclared variable or parameter '%s' (line: %d)\n", token, compiler->context->line);
                }
                else
                {
                    error("You cannot use '%s' as assignment target (line: %d)\n", token, compiler->context->line);
                }
            }
        }
    }
    compiler->context->currentAssignmentTarget++;
    memset(compiler->context->assignmentTargets[compiler->context->currentAssignmentTarget], '\0', TOKEN_SIZE);
    strncpy(compiler->context->assignmentTargets[compiler->context->currentAssignmentTarget], token, TOKEN_SIZE);
}

void discardAssignmentTarget()
{
    VERBOSE("Discarding assignment target '%s'\n", compiler->context->assignmentTargets[compiler->context->currentAssignmentTarget]);
    compiler->context->currentAssignmentTarget--;
}

void insertFunction()
{
    VERBOSE("Inserting function '%s'\n", compiler->context->token);
    if (
        1 == SymbolTable_symbolExists(compiler->symbolTable, compiler->context->token, SYMBOL_PROCEDURE, compiler->context->lexicalLevel)
        || 1 == SymbolTable_symbolExists(compiler->symbolTable, compiler->context->token, SYMBOL_FUNCTION, compiler->context->lexicalLevel)
    )
    {
        error("Redeclaration of procedure/function '%s'\n", compiler->context->token);
    }

    SymbolTableEntry entry = createSymbolTableEntry_function(compiler->context->token, compiler->context->lexicalLevel, LabelStack_get(compiler->context->labels, 1));
    SymbolTableEntry *inserted = SymbolTable_push(compiler->symbolTable, entry);
    compiler->context->currentPOF++;
    compiler->context->POF[compiler->context->currentPOF] = inserted;
}

void insertProcedure()
{
    VERBOSE("Inserting procedure '%s'\n", compiler->context->token);
    if (
        1 == SymbolTable_symbolExists(compiler->symbolTable, compiler->context->token, SYMBOL_PROCEDURE, compiler->context->lexicalLevel)
        || 1 == SymbolTable_symbolExists(compiler->symbolTable, compiler->context->token, SYMBOL_FUNCTION, compiler->context->lexicalLevel)
    )
    {
        error("Redeclaration of procedure/function '%s'\n", compiler->context->token);
    }

    SymbolTableEntry entry = createSymbolTableEntry_procedure(compiler->context->token, compiler->context->lexicalLevel, LabelStack_get(compiler->context->labels, 1));
    SymbolTableEntry *inserted = SymbolTable_push(compiler->symbolTable, entry);
    compiler->context->currentPOF++;
    compiler->context->POF[compiler->context->currentPOF] = inserted;
}

void insertParameter(ParameterType parameterType)
{
    VERBOSE("Inserting parameter '%s'\n", compiler->context->token);
    if (1 == SymbolTable_symbolExists(compiler->symbolTable, compiler->context->token, SYMBOL_PARAMETER, compiler->context->lexicalLevel))
    {
        error("Redeclaration of '%s'\n", compiler->context->token);
    }

    compiler->context->parameterCount[compiler->context->lexicalLevel]++;
    SymbolTableEntry entry = createSymbolTableEntry_parameter(compiler->context->token, compiler->context->lexicalLevel, parameterType);
    SymbolTable_push(compiler->symbolTable, entry);
    SymbolTableEntry_addParameter(compiler->context->POF[compiler->context->currentPOF], parameterType);
}

void insertVariable()
{
    VERBOSE("Inserting variable '%s'\n", compiler->context->token);
    if (1 == SymbolTable_symbolExists(compiler->symbolTable, compiler->context->token, SYMBOL_VARIABLE, compiler->context->lexicalLevel))
    {
        error("Redeclaration of '%s'\n", compiler->context->token);
    }

    compiler->context->offset[compiler->context->lexicalLevel]++;
    SymbolTableEntry entry = createSymbolTableEntry_variable(compiler->context->token, compiler->context->lexicalLevel, compiler->context->offset[compiler->context->lexicalLevel]);
    SymbolTable_push(compiler->symbolTable, entry);
}

void insertLabel()
{
    VERBOSE("Inserting label '%s'\n", compiler->context->token);
    if (1 == SymbolTable_symbolExists(compiler->symbolTable, compiler->context->token, SYMBOL_LABEL, compiler->context->lexicalLevel))
        error("Redeclaration of label '%s'\n", compiler->context->token);

    newLabels(1);
    char *label = LabelStack_get(compiler->context->labels, 1);
    SymbolTableEntry entry = createSymbolTableEntry_label(compiler->context->token, compiler->context->lexicalLevel, label);
    SymbolTable_push(compiler->symbolTable, entry);
    discardLabels();
}

DataType tokenToDataType(const char token[TOKEN_SIZE])
{
    if (0 == strncasecmp(token, "boolean", TOKEN_SIZE))
    {
        return BOOLEAN;
    }
    if (0 == strncasecmp(token, "integer", TOKEN_SIZE))
    {
        return INTEGER;
    }
    error("Unsupported data type '%s'\n", token);
}

void updateParametersDataTypes()
{
    VERBOSE("Updating parameters with undefined data types to '%s'\n", compiler->context->token);
    SymbolTable_updateUndefinedTypes(compiler->symbolTable, SYMBOL_PARAMETER, tokenToDataType(compiler->context->token));
    SymbolTableEntry_updateParametersTypes(compiler->context->POF[compiler->context->currentPOF], tokenToDataType(compiler->context->token));
}

int updateParametersOffsets()
{
    VERBOSE("Updating parameters offsets\n");
    return SymbolTable_updateParametersOffsets(compiler->symbolTable);
}

void updateParametersAndFunction()
{
    /* should be called: function f(a: integer; ...): integer {HERE} */
    int offset = updateParametersOffsets();
    VERBOSE("Updating function\n");
    SymbolTable_updateFunctionOffset(compiler->context->POF[compiler->context->currentPOF], offset - 1);
    SymbolTable_updateFunctionReturnType(compiler->context->POF[compiler->context->currentPOF], tokenToDataType(compiler->context->token));
}

void updateVariablesDataTypes()
{
    VERBOSE("Updating variables with undefined data types to '%s'\n", compiler->context->token);
    SymbolTable_updateUndefinedTypes(compiler->symbolTable, SYMBOL_VARIABLE, tokenToDataType(compiler->context->token));
}

void updateFunctionReturnType()
{
    VERBOSE("Updating functions with undefined return types to '%s'\n", compiler->context->token);
    SymbolTable_updateUndefinedTypes(compiler->symbolTable, SYMBOL_FUNCTION, tokenToDataType(compiler->context->token));
}



/* Labels */
void newLabels(int n)
{
    LabelStack_push(compiler->context->labels, n);
}

void discardLabels()
{
    LabelStack_pop(compiler->context->labels);
}



/* validation */
void assertFunctionParameterCount()
{
    const char *callTarget = compiler->context->callTargets[compiler->context->currentCallTarget];
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, callTarget, SYMBOL_FUNCTION);
    if (match == NULL)
    {
        error("Symbol '%s' is not a function or procedure\n", callTarget);
    }

    if (match->function.parametersLength != compiler->context->counters[compiler->context->currentCounter])
    {
        error("Function '%s' expects %d parameters, but %d were given\n", callTarget, match->function.parametersLength, compiler->context->counters[compiler->context->currentCounter]);
    }
}

void assertFunctionHasNoParameters()
{
    const char *callTarget = compiler->context->callTargets[compiler->context->currentCallTarget];
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, callTarget, SYMBOL_FUNCTION);
    if (match == NULL)
    {
        error("Symbol '%s' is not a function or procedure\n", callTarget);
    }

    if (match->function.parametersLength > 0)
    {
        error("Function '%s' expects %d parameters, but none were given\n", callTarget, match->function.parametersLength);
    }
}

void assertFunctionParameterIs(ParameterType parameterType, DataType dataType)
{
    const char *callTarget = compiler->context->callTargets[compiler->context->currentCallTarget];
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, callTarget, SYMBOL_FUNCTION);
    if (match == NULL)
    {
        error("Symbol '%s' is not a function or procedure\n", callTarget);
    }

    if (!match->function.parameters[compiler->context->counters[compiler->context->currentCounter] - 1].type == dataType)
    {
        error("Parameter %d of function '%s' must be of type '%s' (given: '%s')\n", compiler->context->counters[compiler->context->currentCounter], callTarget, DataTypeName[match->function.parameters[compiler->context->counters[compiler->context->currentCounter] - 1].type], DataTypeName[dataType]);
    }

    if (parameterType == BOTH) return;
    if (match->function.parameters[compiler->context->counters[compiler->context->currentCounter] - 1].parameterType != parameterType)
    {
        if (match->function.parameters[compiler->context->counters[compiler->context->currentCounter] - 1].parameterType == BY_REFERENCE)
        {
            error("Parameter %d of function '%s' is a reference parameter (expression given)\n", compiler->context->counters[compiler->context->currentCounter], callTarget);
        }
    }
}

void assertProcedureParameterCount()
{
    const char *callTarget = compiler->context->callTargets[compiler->context->currentCallTarget];
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, callTarget, SYMBOL_PROCEDURE);
    if (match == NULL)
    {
        error("Symbol '%s' is not a function or procedure\n", callTarget);
    }

    if (match->procedure.parametersLength != compiler->context->counters[compiler->context->currentCounter])
    {
        error("Procedure '%s' expects %d parameters, but %d were given\n", callTarget, match->procedure.parametersLength, compiler->context->counters[compiler->context->currentCounter]);
    }
}

void assertProcedureHasNoParameters()
{
    const char *callTarget = compiler->context->callTargets[compiler->context->currentCallTarget];
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, callTarget, SYMBOL_PROCEDURE);
    if (match == NULL)
    {
        error("Symbol '%s' is not a function or procedure\n", callTarget);
    }

    if (match->procedure.parametersLength > 0)
    {
        error("Procedure '%s' expects %d parameters, but none were given\n", callTarget, match->procedure.parametersLength);
    }
}

void assertProcedureParameterIs(ParameterType parameterType, DataType dataType)
{
    const char *callTarget = compiler->context->callTargets[compiler->context->currentCallTarget];
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, callTarget, SYMBOL_PROCEDURE);
    if (match == NULL)
    {
        error("Symbol '%s' is not a function or procedure\n", callTarget);
    }

    if (!match->procedure.parameters[compiler->context->counters[compiler->context->currentCounter] - 1].type == dataType)
    {
        error("Parameter %d of procedure '%s' is of type '%s' (given: '%s')\n", compiler->context->counters[compiler->context->currentCounter], callTarget, DataTypeName[match->procedure.parameters[compiler->context->counters[compiler->context->currentCounter] - 1].type], DataTypeName[dataType]);
    }

    if (parameterType == BOTH) return;
    if (match->procedure.parameters[compiler->context->counters[compiler->context->currentCounter] - 1].parameterType != parameterType)
    {
        if (match->procedure.parameters[compiler->context->counters[compiler->context->currentCounter] - 1].parameterType == BY_REFERENCE)
        {
            error("Parameter %d of procedure '%s' is a reference parameter (expression given)\n", compiler->context->counters[compiler->context->currentCounter], callTarget);
        }
    }
}

void assertFunctionAssignmentIsValid()
{
    SymbolTableEntry *assignmentTarget = SymbolTable_search(compiler->symbolTable, compiler->context->assignmentTargets[compiler->context->currentAssignmentTarget], SYMBOL_FUNCTION);
    if (assignmentTarget == NULL) error("This shouldn't happen ever... (line: %d)\n", compiler->context->line);
    SymbolTableEntry *POF = compiler->context->POF[assignmentTarget->base.lexicalLevel];
    if (POF->base.category != SYMBOL_FUNCTION) error("This shouldn't happen ever... (line: %d)\n", compiler->context->line);
    if (POF == NULL) error("This shouldn't happen ever... (line: %d)\n", compiler->context->line);
    if (POF != assignmentTarget)
        error("You cannot assign a value to function '%s' in the current context (inside '%s', line: %d)\n", assignmentTarget->base.identifier, POF->base.identifier, compiler->context->line);
}

void assertLabelIsValid()
{
    SymbolTableEntry *label = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_LABEL);
    if (label == NULL) error("Undeclared label '%s' (line: %d)\n", compiler->context->token, compiler->context->line);
    if (label->base.lexicalLevel != compiler->context->lexicalLevel) error("You cannot use the label '%s' in this context (line: %d)\n", compiler->context->token, compiler->context->line);
}



/* Helpers */
void error(const char *message, ...)
{
    va_list args;
    va_start(args, message);
    vfprintf(stderr, message, args);
    exit(1);
}
