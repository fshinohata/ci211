%{
#include <stdio.h>
#include "compiler.h"

void yyerror(const char *s);
int yylex(void);
%}

%token
    /* special */
    T_DEBUG
    T_READ
    T_WRITE

    /* keywords */
    T_PROGRAM
    T_LABEL
    T_VAR
    T_VAR_TYPE
    T_PROCEDURE
    T_FUNCTION
    T_BEGIN
    T_END
    T_GOTO

    /* identifier types */
    T_IDENTIFIER_VARIABLE_UNDEFINED
    T_IDENTIFIER_VARIABLE_INTEGER
    T_IDENTIFIER_VARIABLE_BOOLEAN
    T_IDENTIFIER_FUNCTION_INTEGER
    T_IDENTIFIER_FUNCTION_BOOLEAN
    T_IDENTIFIER_PROCEDURE


    /* separators */
    T_COMMA
    T_SEMICOLON
    T_COLON
    T_DOT
    T_LEFT_PARENTHESIS
    T_RIGHT_PARENTHESIS

    /* conditional and comparison */
    T_IF
    T_THEN
    T_ELSE
    T_NOT_EQUAL
    T_LESS
    T_LESS_OR_EQUAL
    T_EQUAL
    T_GREATER_OR_EQUAL
    T_GREATER

    /* loops */
    T_WHILE
    T_DO
    
    /* logical symbols */
    T_AND
    T_OR
    T_NOT
    T_TRUE
    T_FALSE

    /* math */
    T_PLUS
    T_MINUS
    T_SLASH
    T_ASTERISK
    T_DIV

    /* program */
    T_ASSIGNMENT
    T_NUMBER
    T_IDENTIFIER

%define parse.error verbose

%nonassoc PREC_THEN
%nonassoc PREC_ELSE

%%

r_program:  { INPP(); }
            T_PROGRAM T_IDENTIFIER T_LEFT_PARENTHESIS r_identifier_list T_RIGHT_PARENTHESIS T_SEMICOLON
            r_block T_DOT
            { PARA(); }
;

r_identifier_list: r_identifier_list T_COMMA r_identifier | r_identifier
;
r_identifier: T_IDENTIFIER {count();}
;
r_number: T_NUMBER {CRCT();}
;
r_boolean: T_TRUE {CRCT();} | T_FALSE {CRCT();}
;

r_block: r_label_list_optional
         /*r_type_list_optional*/
         r_var_list_optional
         {newLabels(1);DSVS(1);}
         r_subroutines
         {LABEL(1);discardLabels();}
         r_compound_command
         {DMEM();}
;

r_label_list_optional: r_label_list |
;
r_label_list: T_LABEL r_label_numbers_list T_SEMICOLON
;
r_label_numbers_list: T_NUMBER E_INSERT_LABEL T_COMMA r_label_numbers_list | T_NUMBER E_INSERT_LABEL
;
E_INSERT_LABEL: {insertLabel();} ;

/*
r_type_list_optional: r_type_list |
;
r_type_list:;
*/

r_var_list_optional: r_var_list |
;
r_var_list: T_VAR FLEX_DONT_RETURN_TYPES r_var_declaration_list FLEX_DO_RETURN_TYPES
;
r_var_declaration_list: {addCounter();} r_var_identifier_list {AMEM();discardCounter();} T_COLON r_var_type T_SEMICOLON r_var_declaration_list |
;
r_var_type: T_VAR_TYPE {updateVariablesDataTypes();}
;
r_var_identifier_list: r_var_identifier | r_var_identifier T_COMMA r_var_identifier_list
;
r_var_identifier: r_identifier {insertVariable();}
;

r_subroutines: r_procedure_declaration r_subroutines
             | r_function_declaration r_subroutines
             |
;
r_function_declaration: T_FUNCTION E_ENTER_PROCEDURE_DECLARATION r_function_declaration_identifier T_COLON T_VAR_TYPE {updateParametersAndFunction();} T_SEMICOLON r_block T_SEMICOLON E_END_PROCEDURE_DECLARATION
                      | T_FUNCTION E_ENTER_PROCEDURE_DECLARATION r_function_declaration_identifier T_LEFT_PARENTHESIS r_procedure_declaration_parameter_lists T_RIGHT_PARENTHESIS T_COLON T_VAR_TYPE {updateParametersAndFunction();} T_SEMICOLON r_block T_SEMICOLON E_END_PROCEDURE_DECLARATION
;
r_function_declaration_identifier: T_IDENTIFIER {newLabels(1);LABEL(1);ENPR();insertFunction();discardLabels();}
;
r_procedure_declaration: T_PROCEDURE E_ENTER_PROCEDURE_DECLARATION r_procedure_declaration_identifier T_SEMICOLON r_block T_SEMICOLON E_END_PROCEDURE_DECLARATION
                       | T_PROCEDURE E_ENTER_PROCEDURE_DECLARATION r_procedure_declaration_identifier T_LEFT_PARENTHESIS r_procedure_declaration_parameter_lists T_RIGHT_PARENTHESIS {updateParametersOffsets();} T_SEMICOLON r_block T_SEMICOLON E_END_PROCEDURE_DECLARATION
;
E_ENTER_PROCEDURE_DECLARATION: {increaseLexicalLevel();}
;
E_END_PROCEDURE_DECLARATION: {RTPR();decreaseLexicalLevel();}
;
r_procedure_declaration_identifier: T_IDENTIFIER {newLabels(1);LABEL(1);ENPR();insertProcedure();discardLabels();}
;
r_procedure_declaration_parameter_lists: FLEX_DONT_RETURN_TYPES r_procedure_declaration_copy_parameter_list T_COLON r_procedure_declaration_parameter_list_data_type FLEX_DO_RETURN_TYPES
                                       | FLEX_DONT_RETURN_TYPES r_procedure_declaration_copy_parameter_list T_COLON r_procedure_declaration_parameter_list_data_type T_SEMICOLON r_procedure_declaration_parameter_lists FLEX_DO_RETURN_TYPES
                                       | FLEX_DONT_RETURN_TYPES T_VAR r_procedure_declaration_reference_parameter_list T_COLON r_procedure_declaration_parameter_list_data_type FLEX_DO_RETURN_TYPES
                                       | FLEX_DONT_RETURN_TYPES T_VAR r_procedure_declaration_reference_parameter_list T_COLON r_procedure_declaration_parameter_list_data_type T_SEMICOLON r_procedure_declaration_parameter_lists FLEX_DO_RETURN_TYPES
;
r_procedure_declaration_parameter_list_data_type: T_VAR_TYPE {updateParametersDataTypes();}
;
r_procedure_declaration_copy_parameter_list: r_procedure_declaration_copy_parameter T_COMMA r_procedure_declaration_copy_parameter_list | r_procedure_declaration_copy_parameter
;
r_procedure_declaration_copy_parameter: T_IDENTIFIER {insertParameter(BY_COPY);}
;
r_procedure_declaration_reference_parameter_list: r_procedure_declaration_reference_parameter T_COMMA r_procedure_declaration_reference_parameter_list | r_procedure_declaration_reference_parameter
;
r_procedure_declaration_reference_parameter: T_IDENTIFIER {insertParameter(BY_REFERENCE);}
;

r_compound_command: T_BEGIN r_command_list T_END
;
r_command_list: r_labeled_command T_SEMICOLON r_command_list | r_labeled_command |
;
r_labeled_command: T_NUMBER E_MARK_LABEL T_COLON r_command | r_command
;
E_MARK_LABEL: {assertLabelIsValid();ENRT();}
;
r_command: r_assignment
         | r_call
         | r_goto
         | r_compound_command
         | r_conditional
         | r_loop
         | r_special_command
;

r_special_command: r_read
                 | r_write
;
r_read: T_READ T_LEFT_PARENTHESIS r_read_assignment_targets T_RIGHT_PARENTHESIS
;
r_read_assignment_targets: r_read_assignment_target T_COMMA r_read_assignment_targets | r_read_assignment_target
;
r_read_assignment_target: T_IDENTIFIER_VARIABLE_INTEGER {saveAssignmentTarget();LEIT();store();discardAssignmentTarget();}
                        | T_IDENTIFIER_VARIABLE_BOOLEAN {saveAssignmentTarget();LEIT();store();discardAssignmentTarget();}
;
r_write: T_WRITE T_LEFT_PARENTHESIS r_write_targets T_RIGHT_PARENTHESIS
;
r_write_targets: r_write_target T_COMMA r_write_targets | r_write_target
;
r_write_target: r_math_identifier {IMPR();}
              | r_boolean_identifier {IMPR();}
              | r_math_expression {IMPR();}
              | r_boolean_expression {IMPR();}
              | r_integer_function_call {IMPR();}
              | r_boolean_function_call {IMPR();}
;

r_goto: T_GOTO T_NUMBER {DSVR();}
;

r_loop: T_WHILE {newLabels(2);LABEL(1);} r_boolean_expression {DSVF(2);} T_DO r_command {DSVS(1);LABEL(2);discardLabels();}
;

r_conditional: T_IF r_boolean_expression T_THEN E_CONDITIONAL_LABELS r_command {LABEL(1);discardLabels();} %prec PREC_THEN
             | T_IF r_boolean_expression T_THEN E_CONDITIONAL_LABELS r_command T_ELSE {DSVS(2);LABEL(1);} r_command {LABEL(2);discardLabels();}
;
E_CONDITIONAL_LABELS: {newLabels(2);DSVF(1);}
;

r_call: r_procedure_call
      | r_integer_function_call
      | r_boolean_function_call
;
r_integer_function_call: r_integer_function_identifier T_LEFT_PARENTHESIS {addCounter();saveFunctionSpace();enterCall();} r_function_call_parameter_list T_RIGHT_PARENTHESIS {exitCall();assertFunctionParameterCount();CHPR();discardCounter();}
                       | r_integer_function_identifier {assertFunctionHasNoParameters();CHPR();discardCallTarget();}
;
r_integer_function_identifier: T_IDENTIFIER_FUNCTION_INTEGER {saveCallTarget();}
;
r_boolean_function_call: r_boolean_function_identifier T_LEFT_PARENTHESIS {addCounter();saveFunctionSpace();enterCall();} r_function_call_parameter_list T_RIGHT_PARENTHESIS {exitCall();assertFunctionParameterCount();CHPR();discardCounter();}
                       | r_boolean_function_identifier {assertFunctionHasNoParameters();CHPR();discardCallTarget();}
;
r_boolean_function_identifier: T_IDENTIFIER_FUNCTION_BOOLEAN {saveCallTarget();}
;
r_function_call_parameter_list: r_function_call_parameter T_COMMA r_function_call_parameter_list
                              | r_function_call_parameter
;
r_function_call_parameter: E_COUNT r_math_expression {assertFunctionParameterIs(BY_COPY, INTEGER);}
                         | E_COUNT r_boolean_expression {assertFunctionParameterIs(BY_COPY, BOOLEAN);}
                         | E_COUNT r_math_identifier {assertFunctionParameterIs(BOTH, INTEGER);}
                         | E_COUNT r_boolean_identifier {assertFunctionParameterIs(BOTH, BOOLEAN);}
                         | E_COUNT r_integer_function_call {assertFunctionParameterIs(BY_COPY, INTEGER);}
                         | E_COUNT r_boolean_function_call {assertFunctionParameterIs(BY_COPY, BOOLEAN);}
;
r_procedure_call: r_procedure_identifier T_LEFT_PARENTHESIS {addCounter();enterCall();} r_procedure_call_parameter_list T_RIGHT_PARENTHESIS {exitCall();assertProcedureParameterCount();discardCounter();CHPR();discardCallTarget();}
                | r_procedure_identifier {assertProcedureHasNoParameters();CHPR();discardCallTarget();}
;
r_procedure_identifier: T_IDENTIFIER_PROCEDURE {saveCallTarget();}
;
r_procedure_call_parameter_list: r_procedure_call_parameter T_COMMA r_procedure_call_parameter_list
                               | r_procedure_call_parameter
;
r_procedure_call_parameter: E_COUNT r_math_expression {assertProcedureParameterIs(BY_COPY, INTEGER);}
                          | E_COUNT r_boolean_expression {assertProcedureParameterIs(BY_COPY, BOOLEAN);}
                          | E_COUNT r_math_identifier {assertProcedureParameterIs(BOTH, INTEGER);}
                          | E_COUNT r_boolean_identifier {assertProcedureParameterIs(BOTH, BOOLEAN);}
                          | E_COUNT r_integer_function_call {assertFunctionParameterIs(BY_COPY, INTEGER);}
                          | E_COUNT r_boolean_function_call {assertFunctionParameterIs(BY_COPY, BOOLEAN);}
;
E_COUNT: {count();}
;

r_assignment: r_integer_assignment | r_boolean_assignment | r_integer_function_assignment | r_boolean_function_assignment
;
r_integer_assignment: r_math_assignment_target T_ASSIGNMENT r_math_expression {store();discardAssignmentTarget();};
r_boolean_assignment: r_boolean_assignment_target T_ASSIGNMENT r_boolean_expression {store();discardAssignmentTarget();};
r_integer_function_assignment: r_integer_function_assignment_target T_ASSIGNMENT r_math_expression {store();discardAssignmentTarget();};
r_boolean_function_assignment: r_boolean_function_assignment_target T_ASSIGNMENT r_boolean_expression {store();discardAssignmentTarget();};
r_integer_function_assignment_target: T_IDENTIFIER_FUNCTION_INTEGER {saveAssignmentTarget();assertFunctionAssignmentIsValid();};
r_boolean_function_assignment_target: T_IDENTIFIER_FUNCTION_BOOLEAN {saveAssignmentTarget();assertFunctionAssignmentIsValid();};

r_math_expression: r_math_term T_PLUS r_math_expression {SOMA();}
                 | r_math_term T_MINUS r_math_expression {SUBT();}
                 | r_math_term
;
r_math_term: r_math_term T_ASTERISK r_math_factor {MULT();}
           | r_math_term T_SLASH r_math_factor {DIVI();}
           | r_math_term T_DIV r_math_factor {DIVI();}
           | r_math_factor
;
r_math_factor: r_math_identifier | r_number | r_integer_function_call | T_LEFT_PARENTHESIS r_math_expression T_RIGHT_PARENTHESIS
;
r_math_assignment_target: T_IDENTIFIER_VARIABLE_INTEGER {saveAssignmentTarget();}
;
r_math_identifier: T_IDENTIFIER_VARIABLE_INTEGER {load();}
;

r_boolean_expression: r_boolean_term T_OR r_boolean_term {DISJ();}
                    | r_boolean_term
;
r_boolean_term: r_boolean_factor T_AND r_boolean_factor {CONJ();}
              | r_boolean_factor
;
r_boolean_factor: r_boolean_comparison
                | T_NOT r_boolean_comparison {NEGA();}
                | r_boolean_identifier
                | r_boolean
                | T_LEFT_PARENTHESIS r_boolean_expression T_RIGHT_PARENTHESIS
                | T_NOT T_LEFT_PARENTHESIS r_boolean_expression T_RIGHT_PARENTHESIS {NEGA();}
;
r_boolean_comparison: r_math_expression T_NOT_EQUAL r_math_expression {CMDG();}
                    | r_math_expression T_LESS r_math_expression {CMME();}
                    | r_math_expression T_LESS_OR_EQUAL r_math_expression {CMEG();}
                    | r_math_expression T_EQUAL r_math_expression {CMIG();}
                    | r_math_expression T_GREATER_OR_EQUAL r_math_expression {CMAG();}
                    | r_math_expression T_GREATER r_math_expression {CMMA();}
                    | r_boolean_expression T_NOT_EQUAL r_boolean_expression {CMDG();}
                    | r_boolean_expression T_LESS r_boolean_expression {CMME();}
                    | r_boolean_expression T_LESS_OR_EQUAL r_boolean_expression {CMEG();}
                    | r_boolean_expression T_EQUAL r_boolean_expression {CMIG();}
                    | r_boolean_expression T_GREATER_OR_EQUAL r_boolean_expression {CMAG();}
                    | r_boolean_expression T_GREATER r_boolean_expression {CMMA();}
;
r_boolean_assignment_target: T_IDENTIFIER_VARIABLE_BOOLEAN {saveAssignmentTarget();}
;
r_boolean_identifier: T_IDENTIFIER_VARIABLE_BOOLEAN {load();}
                    | T_NOT T_IDENTIFIER_VARIABLE_BOOLEAN {load();NEGA();}
;

FLEX_DO_RETURN_TYPES: {doReturnTypes();};
FLEX_DONT_RETURN_TYPES: {dontReturnTypes();};

%%

void yyerror(const char *s)
{
    printf("Erro na linha: %d (%s)\nToken: \"%s\", Symbol: %d\n", compiler->context->line + 1, s, compiler->context->token, compiler->context->symbol);
}

int main(void)
{
    initCompiler();
    yyparse();
    deleteCompiler();
    return 0;
}