#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bison.h"

#define SYMBOL_TABLE_STACK_SIZE 1024
#define MAXIMUM_PARAMETER_COUNT 128

void SymbolTableEntry_copy(SymbolTableEntry *dest, SymbolTableEntry *src)
{
    memcpy(dest, src, sizeof(SymbolTableEntry));
}

void SymbolTableEntry_addParameter(SymbolTableEntry *entry, ParameterType parameterType)
{
    switch (entry->base.category)
    {
        case SYMBOL_PROCEDURE:
            entry->procedure.parameters[entry->procedure.parametersLength] = (ProcedureParameter){
                .type = UNDEFINED,
                .parameterType = parameterType
            };
            entry->procedure.parametersLength++;
            break;
        case SYMBOL_FUNCTION:
            entry->function.parameters[entry->function.parametersLength] = (ProcedureParameter){
                .type = UNDEFINED,
                .parameterType = parameterType
            };
            entry->function.parametersLength++;
    }
}

void SymbolTableEntry_updateParametersTypes(SymbolTableEntry *entry, DataType dataType)
{
    switch (entry->base.category)
    {
        case SYMBOL_PROCEDURE:
            for (int i = entry->procedure.parametersLength - 1; i >= 0 && entry->procedure.parameters[i].type == UNDEFINED; i--)
            {
                entry->procedure.parameters[i].type = dataType;
            }
            break;
        case SYMBOL_FUNCTION:
            for (int i = entry->function.parametersLength - 1; i >= 0 && entry->function.parameters[i].type == UNDEFINED; i--)
            {
                entry->function.parameters[i].type = dataType;
            }
            break;
    }
}

SymbolTableEntry createSymbolTableEntry_function(char identifier[TOKEN_SIZE], int lexicalLevel, char label[LABEL_SIZE])
{
    SymbolTableEntry_function f = {
        .base.category = SYMBOL_FUNCTION,
        .base.lexicalLevel = lexicalLevel,
        .parameters = (ProcedureParameter *)malloc(MAXIMUM_PARAMETER_COUNT * sizeof(ProcedureParameter)),
        .parametersLength = 0,
        .returnType = UNDEFINED
    };
    strncpy(f.base.identifier, identifier, TOKEN_SIZE);
    strncpy(f.label, label, LABEL_SIZE);
    return (SymbolTableEntry)f;
}

SymbolTableEntry createSymbolTableEntry_procedure(char identifier[TOKEN_SIZE], int lexicalLevel, char label[LABEL_SIZE])
{
    SymbolTableEntry_procedure p = {
        .base.category = SYMBOL_PROCEDURE,
        .base.lexicalLevel = lexicalLevel,
        .parameters = (ProcedureParameter *)malloc(MAXIMUM_PARAMETER_COUNT * sizeof(ProcedureParameter)),
        .parametersLength = 0
    };
    strncpy(p.base.identifier, identifier, TOKEN_SIZE);
    strncpy(p.label, label, LABEL_SIZE);
    return (SymbolTableEntry)p;
}

SymbolTableEntry createSymbolTableEntry_parameter(char identifier[TOKEN_SIZE], int lexicalLevel, ParameterType parameterType)
{
    SymbolTableEntry_parameter p = {
        .base.category = SYMBOL_PARAMETER,
        .base.lexicalLevel = lexicalLevel,
        .type = UNDEFINED,
        .parameterType = parameterType
    };
    strncpy(p.base.identifier, identifier, TOKEN_SIZE);
    return (SymbolTableEntry)p;
}

SymbolTableEntry createSymbolTableEntry_label(char identifier[TOKEN_SIZE], int lexicalLevel, char label[LABEL_SIZE])
{
    SymbolTableEntry_label l = {
        .base.category = SYMBOL_LABEL,
        .base.lexicalLevel = lexicalLevel
    };
    strncpy(l.base.identifier, identifier, TOKEN_SIZE);
    strncpy(l.label, label, LABEL_SIZE);
    return (SymbolTableEntry)l;
}

SymbolTableEntry createSymbolTableEntry_variable(char identifier[TOKEN_SIZE], int lexicalLevel, int offset)
{
    SymbolTableEntry_variable v = {
        .base.category = SYMBOL_VARIABLE,
        .base.lexicalLevel = lexicalLevel,
        .base.offset = offset,
        .type = UNDEFINED
    };
    strncpy(v.base.identifier, identifier, TOKEN_SIZE);
    return (SymbolTableEntry)v;
}

SymbolTable *createSymbolTable()
{
    SymbolTable *st = (SymbolTable *)malloc(sizeof(SymbolTable));
    st->stack = (SymbolTableEntry *)malloc(SYMBOL_TABLE_STACK_SIZE * sizeof(SymbolTableEntry));
    st->allocated = SYMBOL_TABLE_STACK_SIZE;
    st->head = -1;
    return st;
}

void deleteSymbolTable(SymbolTable *st)
{
    free(st->stack);
    free(st);
}

void SymbolTable_reallocIfNecessary(SymbolTable *st)
{
    if (st->head + 1 >= st->allocated)
    {
        st->allocated <<= 1;
        st->stack = (SymbolTableEntry *)realloc(st->stack, st->allocated * sizeof(SymbolTableEntry));
    }
}

SymbolTableEntry *SymbolTable_push(SymbolTable *st, SymbolTableEntry entry)
{
    (st->head)++;
    SymbolTable_reallocIfNecessary(st);
    SymbolTableEntry_copy(&st->stack[st->head], &entry);
    return &st->stack[st->head];
}

void SymbolTable_pop(SymbolTable *st)
{
    st->head--;
}

void SymbolTable_decreaseLexicalLevelTo(SymbolTable *st, int lexicalLevel)
{
    while (
        st->head >= 0 &&
            (st->stack[st->head].base.lexicalLevel > (lexicalLevel + 1) && (st->stack[st->head].base.category == SYMBOL_PROCEDURE || st->stack[st->head].base.category == SYMBOL_FUNCTION))
            ||
            (st->stack[st->head].base.lexicalLevel > lexicalLevel && (st->stack[st->head].base.category == SYMBOL_VARIABLE || st->stack[st->head].base.category == SYMBOL_PARAMETER || st->stack[st->head].base.category == SYMBOL_LABEL))
    )
    {
        SymbolTable_pop(st);
    }
}

int SymbolTable_symbolExists(SymbolTable *st, const char identifier[TOKEN_SIZE], SymbolTableEntryType category, int lexicalLevel)
{
    SymbolTableEntry *match = SymbolTable_search(st, identifier, category);
    if (match == NULL) return 0;
    if (match->base.lexicalLevel < lexicalLevel) return 0;
    if (match->base.category != category)
    return 1;
}

SymbolTableEntry *SymbolTable_search(SymbolTable *st, const char identifier[TOKEN_SIZE], SymbolTableEntryType category)
{
    for (int i = st->head; i >= 0; i--)
    {
        if (0 == strncasecmp(st->stack[i].base.identifier, identifier, TOKEN_SIZE) && (SYMBOL_ANY == category || st->stack[i].base.category == category))
        {
            return &st->stack[i];
        }
    }
    return NULL;
}

void SymbolTable_updateUndefinedTypes(SymbolTable *st, SymbolTableEntryType category, DataType type)
{
    for (int i = st->head; i >= 0; i--)
    {
        if (category == st->stack[i].base.category &&
            (
                SYMBOL_VARIABLE == st->stack[i].base.category && UNDEFINED == st->stack[i].variable.type
                || SYMBOL_PARAMETER == st->stack[i].base.category && UNDEFINED == st->stack[i].parameter.type
            )
        )
        {
            st->stack[i].variable.type = type;
        }
        else return;
    }
}

int SymbolTable_updateParametersOffsets(SymbolTable *st)
{
    /* returns last offset used */
    int i, offset;
    for (i = st->head, offset = -4; i >= 0 && st->stack[i].base.category == SYMBOL_PARAMETER; i--, offset--)
        st->stack[i].base.offset = offset;
    return offset + 1;
}

void SymbolTable_updateFunctionOffset(SymbolTableEntry *f, int offset)
{
    f->base.offset = offset;
}

void SymbolTable_updateFunctionReturnType(SymbolTableEntry *f, DataType returnType)
{
    f->function.returnType = returnType;
}