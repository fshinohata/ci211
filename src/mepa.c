#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "compiler.h"

#define LEFT_STRAFE 16
#define STR_SIZE 128
#define STRAFE "                "

typedef enum {
    P1, P2, P3
} Parameter;

const char *toString(const int p, Parameter param)
{
    static char p1[TOKEN_SIZE];
    static char p2[TOKEN_SIZE];
    static char p3[TOKEN_SIZE];

    switch (param)
    {
        case P1:
            memset(p1, '\0', TOKEN_SIZE);
            sprintf(p1, "%d", p);
            return p1;
        case P2:
            memset(p2, '\0', TOKEN_SIZE);
            sprintf(p2, "%d", p);
            return p2;
        case P3:
            memset(p3, '\0', TOKEN_SIZE);
            sprintf(p3, "%d", p);
            return p3;
        default:
            error("[toString()] Unknown parameter '%d'\n", param);
    }
    return NULL;
}

void printCode(const char *label, const char *command, const char *p1, const char *p2, const char *p3)
{
    if (label == NULL)
    {
        printf("%s%s", STRAFE, command);
    }
    else
    {
        int leftStrafe = LEFT_STRAFE - (strlen(label) + 1);
        printf("%s:%.*s%s", label, leftStrafe, STRAFE, command);
    }
    if (p1 != NULL)
    {
        printf(" %s", p1);
        if (p2 != NULL)
        {
            printf(", %s", p2);
            if(p3 != NULL)
            {
                printf(", %s", p3);
            }
        }
    }
    printf("\n");
    fflush(stdout);
}

void DEBUG()
{
    printCode(NULL, "\n" STRAFE "DEBUG", NULL, NULL, NULL);
}

void INPP()
{
    printCode(NULL, "INPP", NULL, NULL, NULL);
}

void PARA()
{
    printCode(NULL, "PARA", NULL, NULL, NULL);
}

void AMEM()
{
    printCode(NULL, "AMEM", toString(compiler->context->counters[compiler->context->currentCounter], P1), NULL, NULL);
}

void DMEM()
{
    int dmem = compiler->context->offset[compiler->context->lexicalLevel] + 1;
    if (dmem > 0)
        printCode(NULL, "DMEM", toString(dmem, P1), NULL, NULL);
}

void SOMA()
{
    printCode(NULL, "SOMA", NULL, NULL, NULL);
}

void SUBT()
{
    printCode(NULL, "SUBT", NULL, NULL, NULL);
}

void MULT()
{
    printCode(NULL, "MULT", NULL, NULL, NULL);
}

void DIVI()
{
    printCode(NULL, "DIVI", NULL, NULL, NULL);
}

void CONJ()
{
    printCode(NULL, "CONJ", NULL, NULL, NULL);
}

void DISJ()
{
    printCode(NULL, "DISJ", NULL, NULL, NULL);
}

void CRCT()
{
    if (compiler->context->symbol == S_NUMBER)
    {
        printCode(NULL, "CRCT", compiler->context->token, NULL, NULL);
    }
    else if (compiler->context->symbol == S_TRUE)
    {
        printCode(NULL, "CRCT", toString(1, P1), NULL, NULL);
    }
    else if (compiler->context->symbol == S_FALSE)
    {
        printCode(NULL, "CRCT", toString(0, P1), NULL, NULL);
    }
}

void CRVL()
{
    SymbolTableEntry *symbol = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_ANY);
    if (symbol->base.category != SYMBOL_VARIABLE && symbol->base.category != SYMBOL_PARAMETER) error("[CRVL] This should never happen...\n");
    printCode(NULL, "CRVL", toString(symbol->base.lexicalLevel, P1), toString(symbol->base.offset, P2), NULL);
}

void CRVI()
{
    SymbolTableEntry *symbol = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_ANY);
    if (symbol->base.category != SYMBOL_VARIABLE && symbol->base.category != SYMBOL_PARAMETER) error("[CRVI] This should never happen...\n");
    printCode(NULL, "CRVI", toString(symbol->base.lexicalLevel, P1), toString(symbol->base.offset, P2), NULL);
}

void CREN()
{
    SymbolTableEntry *symbol = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_ANY);
    if (symbol->base.category != SYMBOL_VARIABLE && symbol->base.category != SYMBOL_PARAMETER) error("[CREN] This should never happen...\n");
    printCode(NULL, "CREN", toString(symbol->base.lexicalLevel, P1), toString(symbol->base.offset, P2), NULL);
}

void ARMZ()
{
    SymbolTableEntry *assignmentTarget = SymbolTable_search(compiler->symbolTable, compiler->context->assignmentTargets[compiler->context->currentAssignmentTarget], SYMBOL_ANY);
    if (assignmentTarget->base.category != SYMBOL_VARIABLE && assignmentTarget->base.category != SYMBOL_PARAMETER && assignmentTarget->base.category != SYMBOL_FUNCTION) error("[ARMZ] This should never happen...\n");
    printCode(NULL, "ARMZ", toString(assignmentTarget->base.lexicalLevel, P1), toString(assignmentTarget->base.offset, P2), NULL);
}

void ARMI()
{
    SymbolTableEntry *assignmentTarget = SymbolTable_search(compiler->symbolTable, compiler->context->assignmentTargets[compiler->context->currentAssignmentTarget], SYMBOL_ANY);
    if (assignmentTarget->base.category != SYMBOL_VARIABLE && assignmentTarget->base.category != SYMBOL_PARAMETER && assignmentTarget->base.category != SYMBOL_FUNCTION) error("[ARMI] This should never happen...\n");
    printCode(NULL, "ARMI", toString(assignmentTarget->base.lexicalLevel, P1), toString(assignmentTarget->base.offset, P2), NULL);
}

void CMDG()
{
    printCode(NULL, "CMDG", NULL, NULL, NULL);
}

void CMME()
{
    printCode(NULL, "CMME", NULL, NULL, NULL);
}

void CMEG()
{
    printCode(NULL, "CMEG", NULL, NULL, NULL);
}

void CMIG()
{
    printCode(NULL, "CMIG", NULL, NULL, NULL);
}

void CMAG()
{
    printCode(NULL, "CMAG", NULL, NULL, NULL);
}

void CMMA()
{
    printCode(NULL, "CMMA", NULL, NULL, NULL);
}

void NEGA()
{
    printCode(NULL, "NEGA", NULL, NULL, NULL);
}

void DSVS(int n)
{
    const char *label = compiler->context->labels->stack[compiler->context->labels->head].names[n - 1];
    printCode(NULL, "DSVS", label, NULL, NULL);
}

void DSVF(int n)
{
    const char *label = compiler->context->labels->stack[compiler->context->labels->head].names[n - 1];
    printCode(NULL, "DSVF", label, NULL, NULL);
}

void LABEL(int n)
{
    const char *label = compiler->context->labels->stack[compiler->context->labels->head].names[n - 1];
    printCode(label, "NADA", NULL, NULL, NULL);
}

void ENPR()
{
    printCode(NULL, "ENPR", toString(compiler->context->lexicalLevel, P1), NULL, NULL);
}

void CHPR()
{
    const char *label;
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, compiler->context->callTargets[compiler->context->currentCallTarget], SYMBOL_PROCEDURE);
    if (match != NULL)
        label = match->procedure.label;
    else
    {
        match = SymbolTable_search(compiler->symbolTable, compiler->context->callTargets[compiler->context->currentCallTarget], SYMBOL_FUNCTION);
        label = match->function.label;
    }
    printCode(NULL, "CHPR", label, toString(compiler->context->lexicalLevel, P2), NULL);
}

void RTPR()
{
    const int parameterCount = compiler->context->parameterCount[compiler->context->lexicalLevel];
    printCode(NULL, "RTPR", toString(compiler->context->lexicalLevel, P1), toString(parameterCount, P2), NULL);
}

void LEIT()
{
    printCode(NULL, "LEIT", NULL, NULL, NULL);
}

void IMPR()
{
    printCode(NULL, "IMPR", NULL, NULL, NULL);
}

void ENRT()
{
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_LABEL);
    if (match == NULL) error("Undeclared label '%s' (line: %d)\n", compiler->context->token, compiler->context->line);
    printCode(match->label.label, "ENRT", toString(compiler->context->lexicalLevel, P1), toString(compiler->context->offset[compiler->context->lexicalLevel] + 1, P2), NULL);
}

void DSVR()
{
    SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_LABEL);
    if (match == NULL) error("Undeclared label '%s' (line: %d)\n", compiler->context->token, compiler->context->line);
    printCode(NULL, "DSVR", match->label.label, toString(match->label.base.lexicalLevel, P2), toString(compiler->context->lexicalLevel, P3));
}
