%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bison.tab.h"
#include "compiler.h"
%}

%option caseless

new_line [\n]
space [\t ]
number [0-9]+
identifier [a-zA-Z][a-zA-Z0-9]*
comment \(\*.*\*\)

%%

{new_line} { compiler->context->line++; }
"(* debug *)" { DEBUG(); }
{comment} {}
{space} {}

"." { return registerToken(S_DOT, T_DOT); }
"," { return registerToken(S_COMMA, T_COMMA); }
";" { return registerToken(S_SEMICOLON, T_SEMICOLON); }
":" { return registerToken(S_COLON, T_COLON);}
"(" { return registerToken(S_LEFT_PARENTHESIS, T_LEFT_PARENTHESIS); }
")" { return registerToken(S_RIGHT_PARENTHESIS, T_RIGHT_PARENTHESIS); }
"+" { return registerToken(S_PLUS, T_PLUS); }
"-" { return registerToken(S_MINUS, T_MINUS); }
"*" { return registerToken(S_ASTERISK, T_ASTERISK); }
"/" { return registerToken(S_SLASH, T_SLASH); }
div { return registerToken(S_DIV, T_DIV); }

and { return registerToken(S_AND, T_AND); }
or  { return registerToken(S_OR, T_OR); }
not { return registerToken(S_NOT, T_NOT); }

":=" { return registerToken(S_ASSIGNMENT, T_ASSIGNMENT); }
"<>" { return registerToken(S_NOT_EQUAL, T_NOT_EQUAL); }
"<"  { return registerToken(S_LESS, T_LESS); }
"<=" { return registerToken(S_LESS_OR_EQUAL, T_LESS_OR_EQUAL); }
"="  { return registerToken(S_EQUAL, T_EQUAL); }
">=" { return registerToken(S_GREATER_OR_EQUAL, T_GREATER_OR_EQUAL); }
">"  { return registerToken(S_GREATER, T_GREATER); }

program { return registerToken(S_PROGRAM, T_PROGRAM); }

label { return registerToken(S_LABEL, T_LABEL); }

var { return registerToken(S_VAR, T_VAR); }

write { return registerToken(S_WRITE, T_WRITE); }

read { return registerToken(S_READ, T_READ); }

procedure { return registerToken(S_PROCEDURE, T_PROCEDURE); }

function { return registerToken(S_FUNCTION, T_FUNCTION); }

begin { return registerToken(S_BEGIN, T_BEGIN); }

end { return registerToken(S_END, T_END); }

while { return registerToken(S_WHILE, T_WHILE); }

do { return registerToken(S_DO, T_DO); }

if { return registerToken(S_IF, T_IF); }

then { return registerToken(S_THEN, T_THEN); }

else { return registerToken(S_ELSE, T_ELSE); }

true { return registerToken(S_TRUE, T_TRUE); }

false { return registerToken(S_FALSE, T_FALSE); }

integer { return registerToken(S_VAR_TYPE, T_VAR_TYPE); }

boolean { return registerToken(S_VAR_TYPE, T_VAR_TYPE); }

goto { return registerToken(S_GOTO, T_GOTO); }

{number} { return registerToken(S_NUMBER, T_NUMBER); }
{identifier} { return registerToken(S_IDENTIFIER, T_IDENTIFIER); }

%%

