#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "flex.h"
#include "bison.tab.h"
#include "compiler.h"

extern char *yytext;
int returnTypes = 1;

void dontReturnTypes()
{
    returnTypes = 0;
}

void doReturnTypes()
{
    returnTypes = 1;
}

enum yytokentype registerToken(Symbol s, enum yytokentype t)
{
    compiler->context->symbol = s;
    strncpy(compiler->context->lastToken, compiler->context->token, TOKEN_SIZE);
    strncpy(compiler->context->token, yytext, TOKEN_SIZE);

    if (s == S_IDENTIFIER && returnTypes == 1)
    {
        SymbolTableEntry *match = SymbolTable_search(compiler->symbolTable, compiler->context->token, SYMBOL_ANY);
        compiler->context->entry = NULL;
        if (match != NULL)
        {
            compiler->context->entry = match;
            switch (match->base.category)
            {
                case SYMBOL_VARIABLE:
                    switch (match->variable.type)
                    {
                        case INTEGER:
                            return T_IDENTIFIER_VARIABLE_INTEGER;
                        case BOOLEAN:
                            return T_IDENTIFIER_VARIABLE_BOOLEAN;
                        case UNDEFINED:
                            return T_IDENTIFIER_VARIABLE_UNDEFINED;
                        default:
                            error("Unknown variable type '%d'\n", match->variable.type);
                    }
                case SYMBOL_PARAMETER:
                    switch (match->parameter.type)
                    {
                        case INTEGER:
                            return T_IDENTIFIER_VARIABLE_INTEGER;
                        case BOOLEAN:
                            return T_IDENTIFIER_VARIABLE_BOOLEAN;
                        default:
                            error("Unknown parameter type '%d'\n", match->variable.type);
                    }
                case SYMBOL_PROCEDURE:
                    return T_IDENTIFIER_PROCEDURE;
                case SYMBOL_FUNCTION:
                    switch (match->function.returnType)
                    {
                        case INTEGER:
                            return T_IDENTIFIER_FUNCTION_INTEGER;
                        case BOOLEAN:
                            return T_IDENTIFIER_FUNCTION_BOOLEAN;
                        default:
                            error("Unknown function return type '%d'\n", match->function.returnType);
                    }
            }
        }
    }

    return t;
}