program test(input, output);
var i, j, k: integer;
begin
    i := 1;
    j := 2;
    k := 10;
    (* debug *)
    if i < j then
        i := 3;
    
    (* debug *)
    if i < j then
        i := 4
    else
        j := 4;

    if i < j then
        (* debug *)
        if j > k then
            k := 20
        else
            j := 20;

end.