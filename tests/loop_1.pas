program test(input, output);
var i, n: integer;
begin
    i := 1;
    n := 20;
    (* debug *)
    while i < n do
        i := i + 1;
    (* debug *)
    i := 1;
    while i < n do
    begin
        i := i + 1;
    end;
end.