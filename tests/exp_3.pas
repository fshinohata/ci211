program a(input, output);
var
    a, b: integer;
    c: boolean;
begin
    a := 1;
    b := 2;
    (* debug *)
    c := true;
    (* debug *)
    c := a > b;
    (* debug *)
    c := a > b and b <> a or true;
    (* debug *)
    c := a > b and (b <> a or true);
end.
