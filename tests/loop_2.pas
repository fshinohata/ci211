program test(input, output);
var i, j, n: integer;
begin
    i := 1;
    j := 1;
    n := 20;
    (* debug *)
    while i < n do
        while j < n do
            i := i + 1;
    (* debug *)
    i := 1;
    while i < n do
    begin
        j := 1;
        while j < n do
            j := j + 1;
        i := i + 1;
    end;
end.